<?php
    class Admin extends CI_Controller{

        function __construct(){
            parent::__construct();
            //$this->load->model('Admin_model', 'admin');
        }

        public function manejo_sesion(){
            if(!$this->session->has_userdata('s_codUsuario') &&
                !$this->session->has_userdata('s_nombreCompleto') &&
                !$this->session->has_userdata('s_tipoUsuario') &&
                !$this->session->has_userdata('Conexion')){
                redirect('Login');
                exit();
            }
        }

        public function index(){
            //$this->manejo_sesion();
            $param = array(
                'page' => 'homeAdministrador',
                'title' => 'UNFV | Admin'
            );
            //$this->load->view('layout/header', $param);
            $this->load->view('layout/header');
            $this->load->view('layout/menu');
            $this->load->view('admin/home');
            $this->load->view('layout/footer');
        }

        public function Estudiante(){
            $this->manejo_sesion();
            $param = array(
                'page' => 'admin/estudiante',
                'title' => 'UNFV | Admin'
            );
            $this->load->view('layout/header', $param);
            $this->load->view('layout/menu');
            $this->load->view('admin/estudiante');
            $this->load->view('layout/footer');
        }

        public function Docente(){
            $this->manejo_sesion();
            $param = array('page' => 'admin/docente');
            $this->load->view('layout/header', $param);
            $this->load->view('layout/menu');
            $this->load->view('admin/docente');
            $this->load->view('layout/footer');
        }

        public function Admin(){
            $this->manejo_sesion();
            $param = array('page' => 'admin/admin',
                           'title' => 'UNFV | Admin');
            $this->load->view('layout/header', $param);
            $this->load->view('layout/menu');
            $this->load->view('admin/admin');
            $this->load->view('layout/footer');
        }

        public function Cursos(){
            $this->manejo_sesion();
            $param = array('page' => 'admin/cursos',
                           'title' => 'UNFV | Admin');
            $this->load->view('layout/header', $param);
            $this->load->view('layout/menu');
            $this->load->view('admin/cursos');
            $this->load->view('layout/footer');
        }

        public function Periodo(){
            $this->manejo_sesion();
            $param = array('page' => 'admin/periodo');
            $this->load->view('layout/header', $param);
            $this->load->view('layout/menu');
            $this->load->view('admin/periodo');
            $this->load->view('layout/footer');
        }

        public function Horario(){
            $this->manejo_sesion();
            $param = array('page' => 'admin/horarios');
            $this->load->view('layout/header', $param);
            $this->load->view('layout/menu');
            $this->load->view('admin/horarios');
            $this->load->view('layout/footer');
        }

        public function Malla(){
            $this->manejo_sesion();
            $param = array('page' => 'admin/malla');
            $this->load->view('layout/header', $param);
            $this->load->view('layout/menu');
            $this->load->view('admin/malla');
            $this->load->view('layout/footer');
        }

        public function agregar(){

          // POST DATOS PERSONALES
          $admin['codAdministrador'] = $this->input->post('codadministrador');
          $admin['dni'] = $this->input->post('dni');
          $admin['apePaterno'] = $this->input->post('apepaterno');
          $admin['apeMaterno'] = $this->input->post('apematerno');
          $admin['nombres'] = $this->input->post('nombres');
          $admin['telefono'] = $this->input->post('celular');
          $admin['sexo'] = $this->input->post('genero');
          $admin['email'] = $this->input->post('email');
          $admin['fechaNacimiento'] = $this->input->post('fecnac');
          $admin['usuarioIngreso'] = $this->session->userdata('s_usuario');
          $admin['tipoUsuario'] = 101;
          $admin['rol'] = $this->input->post('rol');

          $result = $this->admin->agregar($admin);

          if($result != false){
            $res['res'] = 'OK';
          }else{
            $res['res'] = 'FAILED';
          }

          echo json_encode($res);

        }

    }
?>
